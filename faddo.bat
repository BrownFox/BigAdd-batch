@echo off
(setlocal enabledelayedexpansion

    rem find length of two input numbers
    set num1=A%1
    set num2=A%2
    for %%a in (4096 2048 1024 512 256 128 64 32 16 8 4 2 1) do (
        if not "!num1:~%%a!"=="" (
            set num1=!num1:~%%a!
            set /a len1+=%%a
        )
        if not "!num2:~%%a!"=="" (
            set num2=!num2:~%%a!
            set /a len2+=%%a
        )
    )

    rem if first number has more digits than second number, then we iterate the digits of the second number, so when second number is "out" of digits, then we can append the rest of the first number (with carry), for a performance boost. Otherwise, iteraate digits of first number
    if !len1! GTR !len2! (
        set num1=%2
        set num2=%1
    ) else (
        set num1=%1
        set num2=%2
    )

    rem below code to be able to use for %%a in (...), but must then reverse so to go from low to high digits in iteration
    rem idea copied from add.bat, but maybe faster to iterate through individual numbers with substring?
    for %%a in (0 1 2 3 4 5 6 7 8 9) do (
        set num1=!num1:%%a=%%a !
    )

    for %%a in (!num1!) do (
        set "reverse=%%a !reverse!"
    )
    
    rem add until num1 is out of digits
    for %%a in (!reverse!) do (
        set /a temp_result=!carry! + %%a + !num2:~-1,1!
        set num2=!num2:~0,-1!
        set /a temp_var=!temp_result! %% 10,carry=!temp_result!/10
        set "result=!temp_var!!result!"
    )

    rem Now, num1 is out of digits. If num2 and carry are both essentially 0, then end.
    
    rem either append or keep adding

    if !carry!==0 echo !num2!!result! & endlocal & exit/b 0

    if "!num2!"=="" echo 1!result! & endlocal & exit/b 0

    rem set var to be num2 before we destructively modify it
    set temp_num2=!num2!

    rem split and reverse num2 since carry is not 0
    for %%a in (0 1 2 3 4 5 6 7 8 9) do (
        set num2=!num2:%%a=%%a !
    )

    set "reverse= "
    for %%a in (!num2!) do (
        set "reverse=%%a !reverse!"
    )
    
    for %%a in (!reverse!) do (
        set temp_num2=!temp_num2:~0,-1!
        set /a temp_result=%%a + !carry!
        set /a temp_var=!temp_result! %% 10,carry=!temp_result!/10
        set "result=!temp_var!!result!"
        if !carry!==0 echo !temp_num2!!result! & endlocal & exit/b 0
    )
    
    rem num2 out of digits, but carry not 0. carry must be 1, so append and output.
    echo 1!result! & endlocal & exit/b 0
)